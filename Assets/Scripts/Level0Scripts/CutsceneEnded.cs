﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class CutsceneEnded : MonoBehaviour
{
    public PlayableDirector playableDirector;
    public GameObject button;
    public AudioSource music;

	private void Start() 
	{
        playableDirector.stopped += PlayableDirector_stopped;
	}

    private void PlayableDirector_stopped(PlayableDirector obj)
    {
        print("Cutscene 1 ended");
        button.SetActive(true);
        music.Play();
    }
}