﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;


public class ChangeScene : MonoBehaviour
{
    public PlayableDirector playableDirector;
    public AudioSource bitAudioSource;

    private bool isCutsceneEnded = false;


    void Start()
    {
        playableDirector.stopped += PlayableDirector_stopped;
    }

    void Update()
    {

        if (isCutsceneEnded && !bitAudioSource.isPlaying)
        {
            SceneManager.LoadScene("scene1");
        }
    }

    private void PlayableDirector_stopped(PlayableDirector obj)
    {
        Camera.main.transform.position = new Vector3(1000, 1000, 1000);
        bitAudioSource.Play();
        isCutsceneEnded = true;
    }
}
