﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour, IPointerEnterHandler
{
    public int count;
    public AudioSource music;
    public PlayableDirector playableDirector;
    //public GameObject timeline;
    private bool cutscene2Started = false;

    private RectTransform RectTransform { get { return transform as RectTransform; } } // ЧЕСТНО СКОПИПАЩЕННЫЙ КОД.  

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (count < 19)
        {
            RectTransform.anchoredPosition = GetRandomScreenPoint();
            count++;
        }
        else if (count == 19)
        {
            count++;
        }
        else if (count >= 20)
        {
            Destroy(music);
            RectTransform.anchoredPosition = new Vector2(312, 217);

            /*худший код на свете, мой инстаграм - denis.alexeevv подписывайтесь */
            playableDirector.Play();
            //timeline.PlayableDirector.Play();  
        }
    }

    private Vector3 GetRandomScreenPoint()
    {
        var x = Random.Range(0, Screen.width - RectTransform.rect.width);
        var y = Random.Range(0, Screen.height - RectTransform.rect.height);

        return new Vector3(x, y);
    }
}
    